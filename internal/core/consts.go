// Copyright 2022 The Accumulate Authors
//
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.

package core

import (
	"fmt"
	"regexp"

	"github.com/robfig/cron/v3"
	"github.com/tendermint/tendermint/libs/log"
	"gitlab.com/accumulatenetwork/accumulate/private"
	"gitlab.com/accumulatenetwork/core/daemon/internal/node/config"
)

const SnapshotMajorFormat = "snapshot-major-block-%09d.bpt"

var SnapshotMajorRegexp = regexp.MustCompile(`snapshot-major-block-\d+.bpt`)

var Cron = cron.NewParser(cron.SecondOptional | cron.Minute | cron.Hour | cron.Dom | cron.Month | cron.Dow | cron.Descriptor)

// Open opens a key-value store and creates a new database with it.
func OpenDatabase(cfg *config.Config, logger log.Logger) (*private.Database, error) {
	var storeLogger log.Logger
	if logger != nil {
		storeLogger = logger.With("module", "storage")
	}

	var store private.KeyValueStore
	var err error
	switch cfg.Accumulate.Storage.Type {
	case config.MemoryStorage:
		store = private.NewMemoryStore(storeLogger)

	case config.BadgerStorage:
		store, err = private.NewBadgerStore(config.MakeAbsolute(cfg.RootDir, cfg.Accumulate.Storage.Path), storeLogger)

	// case config.EtcdStorage:
	// return OpenEtcd(cfg.Accumulate.PartitionId, cfg.Accumulate.Storage.Etcd, logger)

	default:
		return nil, fmt.Errorf("unknown storage format %q", cfg.Accumulate.Storage.Type)
	}
	if err != nil {
		return nil, err
	}

	return private.NewDatabase(store, logger), nil
}
