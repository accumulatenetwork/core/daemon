// Code generated by mockery v2.14.0. DO NOT EDIT.

package p2p

import (
	context "context"

	peer "github.com/libp2p/go-libp2p/core/peer"
	multiaddr "github.com/multiformats/go-multiaddr"
	mock "github.com/stretchr/testify/mock"
)

// mockDialerPeers is an autogenerated mock type for the dialerPeers type
type mockDialerPeers struct {
	mock.Mock
}

type mockDialerPeers_Expecter struct {
	mock *mock.Mock
}

func (_m *mockDialerPeers) EXPECT() *mockDialerPeers_Expecter {
	return &mockDialerPeers_Expecter{mock: &_m.Mock}
}

// getPeers provides a mock function with given fields: ctx, ma, limit
func (_m *mockDialerPeers) getPeers(ctx context.Context, ma multiaddr.Multiaddr, limit int) (<-chan peer.AddrInfo, error) {
	ret := _m.Called(ctx, ma, limit)

	var r0 <-chan peer.AddrInfo
	if rf, ok := ret.Get(0).(func(context.Context, multiaddr.Multiaddr, int) <-chan peer.AddrInfo); ok {
		r0 = rf(ctx, ma, limit)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(<-chan peer.AddrInfo)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, multiaddr.Multiaddr, int) error); ok {
		r1 = rf(ctx, ma, limit)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// mockDialerPeers_getPeers_Call is a *mock.Call that shadows Run/Return methods with type explicit version for method 'getPeers'
type mockDialerPeers_getPeers_Call struct {
	*mock.Call
}

// getPeers is a helper method to define mock.On call
//   - ctx context.Context
//   - ma multiaddr.Multiaddr
//   - limit int
func (_e *mockDialerPeers_Expecter) getPeers(ctx interface{}, ma interface{}, limit interface{}) *mockDialerPeers_getPeers_Call {
	return &mockDialerPeers_getPeers_Call{Call: _e.mock.On("getPeers", ctx, ma, limit)}
}

func (_c *mockDialerPeers_getPeers_Call) Run(run func(ctx context.Context, ma multiaddr.Multiaddr, limit int)) *mockDialerPeers_getPeers_Call {
	_c.Call.Run(func(args mock.Arguments) {
		run(args[0].(context.Context), args[1].(multiaddr.Multiaddr), args[2].(int))
	})
	return _c
}

func (_c *mockDialerPeers_getPeers_Call) Return(_a0 <-chan peer.AddrInfo, _a1 error) *mockDialerPeers_getPeers_Call {
	_c.Call.Return(_a0, _a1)
	return _c
}

type mockConstructorTestingTnewMockDialerPeers interface {
	mock.TestingT
	Cleanup(func())
}

// newMockDialerPeers creates a new instance of mockDialerPeers. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
func newMockDialerPeers(t mockConstructorTestingTnewMockDialerPeers) *mockDialerPeers {
	mock := &mockDialerPeers{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
