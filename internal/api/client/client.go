// Copyright 2023 The Accumulate Authors
//
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.

package client

import (
	"context"

	"gitlab.com/accumulatenetwork/accumulate/pkg/api/v3"
	"gitlab.com/accumulatenetwork/accumulate/pkg/api/v3/message"
	"gitlab.com/accumulatenetwork/accumulate/pkg/errors"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/core/daemon/internal/api/private"
)

type Client struct {
	*message.Client
}

// PrivateClient is a binary message transport client for private API v3 services.
type PrivateClient Client

// Private returns a [PrivateClient].
func (c Client) Private() private.Sequencer {
	return (PrivateClient)(c)
}

// Sequence implements [private.Sequencer.Sequence].
func (c PrivateClient) Sequence(ctx context.Context, src, dst *url.URL, num uint64) (*api.TransactionRecord, error) {
	req := &message.PrivateSequenceRequest{Source: src, Destination: dst, SequenceNumber: num}

	var typRes *message.PrivateSequenceResponse
	var errRes *message.ErrorResponse
	err := c.RoundTrip(ctx, []message.Message{req}, func(res, _ message.Message) error {
		switch res := res.(type) {
		case *message.ErrorResponse:
			errRes = res
			return nil
		case *message.PrivateSequenceResponse:
			typRes = res
			return nil
		default:
			return errors.Conflict.WithFormat("invalid response type %T", res)
		}
	})
	if err != nil {
		return nil, err
	}
	if errRes != nil {
		return nil, errRes.Error
	}
	return typRes.Value, nil
}
