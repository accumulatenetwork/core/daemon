// Copyright 2023 The Accumulate Authors
//
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.

package main

import (
	"crypto/ed25519"
	"crypto/sha256"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/ghodss/yaml"
	"github.com/spf13/cobra"
	tmed25519 "github.com/tendermint/tendermint/crypto/ed25519"
	"gitlab.com/accumulatenetwork/accumulate/pkg/types/network"
	"gitlab.com/accumulatenetwork/accumulate/private"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/daemon/internal/logging"
	cfg "gitlab.com/accumulatenetwork/core/daemon/internal/node/config"
	accumulated "gitlab.com/accumulatenetwork/core/daemon/internal/node/daemon"
	etcd "go.etcd.io/etcd/client/v3"
)

var cmdInitNetwork = &cobra.Command{
	Use:   "network <network configuration file>",
	Short: "Initialize a network",
	Run:   initNetwork,
	Args:  cobra.ExactArgs(1),
}

func loadNetworkConfiguration(file string) (ret *accumulated.NetworkInit, err error) {
	jsonFile, err := os.Open(file)
	defer func() { _ = jsonFile.Close() }()
	// if we os.Open returns an error then handle it
	if err != nil {
		return ret, err
	}
	data, _ := io.ReadAll(jsonFile)
	err = json.Unmarshal(data, &ret)
	return ret, err
}

// load network config file
func initNetwork(cmd *cobra.Command, args []string) {
	networkConfigFile := args[0]
	network, err := loadNetworkConfiguration(networkConfigFile)
	check(err)

	verifyInitFlags(cmd, len(network.Bvns))

	if flagInit.Reset {
		networkReset()
	}

	for _, bvn := range network.Bvns {
		for _, node := range bvn.Nodes {
			// TODO Check for existing keys?
			node.PrivValKey = tmed25519.GenPrivKey()
			node.DnNodeKey = tmed25519.GenPrivKey()
			node.BvnNodeKey = tmed25519.GenPrivKey()

			if node.ListenAddress == "" {
				node.ListenAddress = "0.0.0.0"
			}
		}
	}

	initNetworkLocalFS(cmd, network)
}

func verifyInitFlags(cmd *cobra.Command, count int) {
	if flagInitDevnet.Compose {
		flagInitDevnet.Docker = true
	}

	if flagInitDevnet.Docker && cmd.Flag("ip").Changed {
		fatalf("--ip and --docker are mutually exclusive")
	}

	if count == 0 {
		fatalf("Must have at least one node")
	}

	switch len(flagInitDevnet.IPs) {
	case 1:
		// Generate a sequence from the base IP
	case count * flagInitDevnet.NumBvns:
		// One IP per node
	default:
		fatalf("not enough IPs - you must specify one base IP or one IP for each node")
	}
}

func initNetworkLocalFS(cmd *cobra.Command, netInit *accumulated.NetworkInit) {
	if flagInit.LogLevels != "" {
		_, _, err := logging.ParseLogLevel(flagInit.LogLevels, io.Discard)
		checkf(err, "--log-level")
	}

	check(os.MkdirAll(flagMain.WorkDir, 0755))

	netFile, err := os.Create(filepath.Join(flagMain.WorkDir, "network.json"))
	check(err)
	enc := json.NewEncoder(netFile)
	enc.SetIndent("", "    ")
	check(enc.Encode(netInit))
	check(netFile.Close())

	var factomAddresses func() (io.Reader, error)
	var snapshots []func() (private.SectionReader, error)
	if flagInit.FactomAddresses != "" {
		factomAddresses = func() (io.Reader, error) { return os.Open(flagInit.FactomAddresses) }
	}
	for _, filename := range flagInit.Snapshots {
		filename := filename // See docs/developer/rangevarref.md
		snapshots = append(snapshots, func() (private.SectionReader, error) { return os.Open(filename) })
	}
	if flagInit.FaucetSeed != "" {
		b := createFaucet(strings.Split(flagInit.FaucetSeed, " "))
		snapshots = append(snapshots, func() (private.SectionReader, error) {
			return private.NewBuffer(b), nil
		})
	}

	values := new(network.GlobalValues)
	if flagInitDevnet.Globals != "" {
		checkf(yaml.Unmarshal([]byte(flagInitDevnet.Globals), values), "--globals")
	}

	genDocs, err := accumulated.BuildGenesisDocs(netInit, values, time.Now(), newLogger(), factomAddresses, snapshots)
	checkf(err, "build genesis documents")

	configs := accumulated.BuildNodesConfig(netInit, nil)
	for _, configs := range configs {
		for _, configs := range configs {
			for _, config := range configs {
				if flagInit.LogLevels != "" {
					config.LogLevel = flagInit.LogLevels
				}

				if flagInit.NoEmptyBlocks {
					config.Consensus.CreateEmptyBlocks = false
				}

				if len(flagInit.Etcd) > 0 {
					config.Accumulate.Storage.Type = cfg.EtcdStorage
					config.Accumulate.Storage.Etcd = new(etcd.Config)
					config.Accumulate.Storage.Etcd.Endpoints = flagInit.Etcd
					config.Accumulate.Storage.Etcd.DialTimeout = 5 * time.Second
				}
			}
		}
	}

	var count int
	dnGenDoc := genDocs[protocol.Directory]
	for i, bvn := range netInit.Bvns {
		bvnGenDoc := genDocs[bvn.Id]
		for j, node := range bvn.Nodes {
			count++
			configs[i][j][0].SetRoot(filepath.Join(flagMain.WorkDir, fmt.Sprintf("node-%d", count), "dnn"))
			configs[i][j][1].SetRoot(filepath.Join(flagMain.WorkDir, fmt.Sprintf("node-%d", count), "bvnn"))

			configs[i][j][0].Config.PrivValidatorKey = "../priv_validator_key.json"
			err = accumulated.WriteNodeFiles(configs[i][j][0], node.PrivValKey, node.DnNodeKey, dnGenDoc)
			checkf(err, "write DNN files")
			configs[i][j][1].Config.PrivValidatorKey = "../priv_validator_key.json"
			err = accumulated.WriteNodeFiles(configs[i][j][1], node.PrivValKey, node.BvnNodeKey, bvnGenDoc)
			checkf(err, "write BVNN files")
		}
	}

	if netInit.Bsn != nil {
		bsnGenDoc := genDocs[netInit.Bsn.Id]
		i := len(netInit.Bvns)
		for j, node := range netInit.Bsn.Nodes {
			configs[i][j][0].SetRoot(filepath.Join(flagMain.WorkDir, fmt.Sprintf("bsn-%d", j+1), "bsnn"))

			configs[i][j][0].Config.PrivValidatorKey = "../priv_validator_key.json"
			err = accumulated.WriteNodeFiles(configs[i][j][0], node.PrivValKey, node.BsnNodeKey, bsnGenDoc)
			checkf(err, "write BSNN files")
		}
	}
}

func createFaucet(seedStrs []string) []byte {
	h := sha256.New()
	for _, s := range seedStrs {
		h.Write([]byte(s))
	}
	sk := ed25519.NewKeyFromSeed(h.Sum(nil))

	lite := protocol.LiteAuthorityForKey(sk[32:], protocol.SignatureTypeED25519)
	b, err := private.CreateLiteFaucet(lite)
	check(err)
	return b
}
